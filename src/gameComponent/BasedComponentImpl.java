package gameComponent;

import java.awt.Image;

import application.Application;
import utilities.Util;

public class BasedComponentImpl implements BasedComponent {

    private int x;
    private int y;
    private int width;
    private int height;

    private Image image;

    public BasedComponentImpl(int x, int y, int width, int height, String imagePath) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = Util.getImage(imagePath);
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void setImage(String imagePath) {
        this.image = Util.getImage(imagePath);
    }

    @Override
    public void move() {
        if (Application.viewport.getxPos() >= 0) {
            this.x = this.x - Application.viewport.getMov();
        }
    }

}
