package gameComponent.objects;

import gameComponent.BasedComponentImpl;
import utilities.ImagePath;

public class Tunnel extends BasedComponentImpl {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 65;

    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT, ImagePath.IMG_TUNNEL);
    }

}
