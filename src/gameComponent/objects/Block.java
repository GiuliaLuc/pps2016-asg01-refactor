package gameComponent.objects;

import gameComponent.BasedComponentImpl;
import utilities.ImagePath;

public class Block extends BasedComponentImpl {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;

    public Block(int x, int y) {
        super(x, y, WIDTH, HEIGHT, ImagePath.IMG_BLOCK);
    }

}
