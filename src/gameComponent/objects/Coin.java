package gameComponent.objects;

import gameComponent.BasedComponentImpl;
import utilities.ImagePath;
import utilities.Util;

import java.awt.Image;

public class Coin extends BasedComponentImpl implements Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private int counter;

    public Coin(int x, int y) {
        super(x, y, WIDTH, HEIGHT, ImagePath.IMG_PIECE1);
    }

    public Image imageOnMovement() {
        return Util.getImage(++this.counter % FLIP_FREQUENCY == 0 ? ImagePath.IMG_PIECE1 : ImagePath.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}
