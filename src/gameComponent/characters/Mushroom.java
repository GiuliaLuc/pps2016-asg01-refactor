package gameComponent.characters;

import java.awt.Image;

import gameComponent.BasedComponentImpl;
import utilities.ImagePath;
import utilities.Util;

public class Mushroom extends BasicCharacter implements Runnable {

    public static final int WIDTH = 27;
    public static final int HEIGHT = 30;

    private final int PAUSE = 15;
    private int offsetX;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT, ImagePath.IMG_MUSHROOM_DEFAULT);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = 1;

        Thread chronoMushroom = new Thread(this);
        chronoMushroom.start();
    }

    public void move() {
        this.offsetX = isToRight() ? 1 : -1;
        this.setX(this.getX() + this.offsetX);

    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive() == true) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(BasedComponentImpl obj) {
        if (this.hitAheadGameObject(obj) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(obj) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public void contact(BasicCharacter pers) {
        if (this.hitAhead(pers) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(pers) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image deadImage() {
        return Util.getImage(this.isToRight() ? ImagePath.IMG_MUSHROOM_DEAD_DX : ImagePath.IMG_MUSHROOM_DEAD_SX);
    }
}
