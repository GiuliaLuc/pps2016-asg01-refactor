package gameComponent.characters;

import java.awt.Image;

import gameComponent.BasedComponentImpl;
import utilities.ImagePath;
import utilities.Util;

public class Turtle extends BasicCharacter implements Runnable {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;


    private final int PAUSE = 15;
    private int dxTurtle;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT,ImagePath.IMG_TURTLE_IDLE );
        super.setToRight(true);
        super.setMoving(true);
        this.dxTurtle = 1;

        Thread chronoTurtle = new Thread(this);
        chronoTurtle.start();
    }

    public void move() {
        this.dxTurtle = isToRight() ? 1 : -1;
        super.setX(super.getX() + this.dxTurtle);
    }

    @Override
    public void run() {
        while (true) {
            if (this.isAlive()) {
                this.move();
                try {
                    Thread.sleep(PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(BasedComponentImpl gameComponent) {
        if (this.hitAheadGameObject(gameComponent) && this.isToRight()) {
            this.setToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(gameComponent) && !this.isToRight()) {
            this.setToRight(true);
            this.dxTurtle = 1;
        }
    }

    public void contact(BasicCharacter character) {
        if (this.hitAhead(character) && this.isToRight()) {
            this.setToRight(false);
            this.dxTurtle = -1;
        } else if (this.hitBack(character) && !this.isToRight()) {
            this.setToRight(true);
            this.dxTurtle = 1;
        }
    }

    public Image deadImage() {
        return Util.getImage(ImagePath.IMG_TURTLE_DEAD);
    }
}
