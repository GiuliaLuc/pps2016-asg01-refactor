package gameComponent.characters;

import java.awt.Image;

import gameComponent.BasedComponentImpl;
import utilities.ImagePath;
import utilities.Util;

public class BasicCharacter extends BasedComponentImpl implements Character  {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int HIT_COSTANT = 5;


    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(int x, int y, int width, int height, String imagePath) {
        super(x, y, width, height, imagePath);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;

    }

    @Override
    public int getCounter() {
        return counter;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public boolean isToRight() {
        return toRight;
    }

    @Override
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public Image walk(String name, int frequency) {
        String str = ImagePath.IMG_BASE + name + (!this.moving || ((this.getCounter()+1) % frequency) == 0 ? ImagePath.IMGP_STATUS_ACTIVE : ImagePath.IMGP_STATUS_NORMAL) +
                (this.toRight ? ImagePath.IMGP_DIRECTION_DX : ImagePath.IMGP_DIRECTION_SX) + ImagePath.IMG_EXT;
        return Util.getImage(str);
    }

    @Override
    public boolean hitAheadGameObject(BasedComponentImpl gameComponent) {
        return !((this.getX() + this.getWidth() < gameComponent.getX() || this.getX() + this.getWidth() > gameComponent.getX() + HIT_COSTANT ||
                this.getY() + this.getHeight() <= gameComponent.getY() || this.getY() >= gameComponent.getY() + gameComponent.getHeight()));

    }

    @Override
    public boolean hitAhead(BasicCharacter character) {
        if (this.isToRight()) {
            return hitAheadGameObject(character);
        } else {
            return false;
        }
    }

    @Override
    public boolean hitBack(BasedComponentImpl gameComponent) {

        return !((this.getX() > gameComponent.getX() + gameComponent.getWidth() || this.getX() + this.getWidth() < gameComponent.getX() + gameComponent.getWidth() - HIT_COSTANT ||
                this.getY() + this.getHeight() <= gameComponent.getY() || this.getY() >= gameComponent.getY() + gameComponent.getHeight()));
    }

    @Override
    public boolean hitBelow(BasedComponentImpl gameComponent) {

        return !((this.getX() + this.getWidth() < gameComponent.getX() + 5 || this.getX() > gameComponent.getX() + gameComponent.getWidth() - HIT_COSTANT ||
                this.getY() + this.getHeight() < gameComponent.getY() || this.getY() + this.getHeight() > gameComponent.getY() + HIT_COSTANT));
    }

    public boolean hitBelow(BasicCharacter character) {
        return !((this.getX() + this.getWidth() < character.getX() || this.getX() > character.getX() + character.getWidth() ||
                this.getY() + this.getHeight() < character.getY() || this.getY() + this.getHeight() > character.getY()));

    }

    @Override
    public boolean hitAbove(BasedComponentImpl gameComponent) {
        return !((this.getX() + this.getWidth() < gameComponent.getX() + 5 || this.getX() > gameComponent.getX() + gameComponent.getWidth() - HIT_COSTANT ||
                this.getY() < gameComponent.getY() + gameComponent.getHeight() || this.getY() > gameComponent.getY() + gameComponent.getHeight() + HIT_COSTANT));
    }

    @Override
    public boolean isNearby(BasedComponentImpl gameComponent) {
        return ((this.getX() > gameComponent.getX() - PROXIMITY_MARGIN && this.getX() < gameComponent.getX() + gameComponent.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > gameComponent.getX() - PROXIMITY_MARGIN && this.getX() + this.getWidth() < gameComponent.getX() + gameComponent.getWidth() + PROXIMITY_MARGIN));
    }
}
