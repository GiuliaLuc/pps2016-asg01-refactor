package gameComponent.characters;

import java.awt.Image;

import application.Application;
import game.Platform;
import game.PlatformImpl;
import gameComponent.BasedComponentImpl;
import gameComponent.objects.Coin;
import utilities.ImagePath;
import utilities.Util;

import javax.swing.*;

public class Mario extends BasicCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;

    private boolean jumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT, ImagePath.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;

    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String imagePath;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Application.viewport.getHeightLimit())
                this.setY(this.getY() - 4);
            else {
                this.jumpingExtent = JUMPING_LIMIT;
            }
            imagePath = this.isToRight() ? ImagePath.IMG_MARIO_SUPER_DX : ImagePath.IMG_MARIO_SUPER_SX;
        } else {
            if (this.getY() + this.getHeight() < Application.viewport.getFloorOffsetY()) {
                this.setY(this.getY() + 1);
                imagePath = this.isToRight() ? ImagePath.IMG_MARIO_SUPER_DX : ImagePath.IMG_MARIO_SUPER_SX;
            } else {
                imagePath = this.isToRight() ? ImagePath.IMG_MARIO_ACTIVE_DX : ImagePath.IMG_MARIO_ACTIVE_SX;
                this.jumping = false;
                this.jumpingExtent = 0;
            }
        }

        return Util.getImage(imagePath);
    }

    public void contactGameComponent(BasedComponentImpl gameComponent) {
        if (this.hitAheadGameObject(gameComponent) && this.isToRight() || this.hitBack(gameComponent) && !this.isToRight()) {
            Application.viewport.setMov(0);
            this.setMoving(false);
        }

        if (this.hitBelow(gameComponent) && this.jumping) {
            Application.viewport.setFloorOffsetY(gameComponent.getY());
        } else {
            if (!this.hitBelow(gameComponent)) {
                Application.viewport.setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
                if (!this.jumping) {
                    this.setY(MARIO_OFFSET_Y_INITIAL);
                }

                if (hitAbove(gameComponent)) {
                    Application.viewport.setHeightLimit(gameComponent.getY() + gameComponent.getHeight()); // the new sky goes below the object
                } else {
                    if (!this.hitAbove(gameComponent) && !this.jumping) {
                        Application.viewport.setHeightLimit(0);
                    }
                }
            }
        }
    }

    public boolean contactPiece(Coin coin) {
        return (this.hitBack(coin) || this.hitAbove(coin) || this.hitAheadGameObject(coin)
                || this.hitBelow(coin));
    }

    public void contactCharacter(BasicCharacter character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
                System.exit(0);
            } else {
                this.setAlive(true);
            }
        } else {
            if (this.hitBelow(character)) {
                character.setMoving(false);
                character.setAlive(false);
            }
        }
    }
}
