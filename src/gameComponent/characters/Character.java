package gameComponent.characters;

import gameComponent.BasedComponentImpl;

import java.awt.Image;

public interface Character {


    int getCounter();

    boolean isAlive();

	boolean isMoving();

	boolean isToRight();

	void setAlive(boolean alive);

	void setMoving(boolean moving);

	void setToRight(boolean toRight);

	void setCounter(int counter);

	Image walk(String name, int frequency);

	boolean hitAheadGameObject(BasedComponentImpl og);

    boolean hitAhead(BasicCharacter character);

    boolean hitBack(BasedComponentImpl og);

	boolean hitBelow(BasedComponentImpl og);

    boolean hitBelow(BasicCharacter character);

	boolean hitAbove(BasedComponentImpl og);

	boolean isNearby(BasedComponentImpl obj);

}
