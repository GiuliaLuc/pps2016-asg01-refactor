package gameComponent;

import java.awt.*;

/**
 * Created by lucch on 17/03/2017.
 */
public interface BasedComponent {


    int getWidth();

    int getHeight();

    int getX();

    int getY();

    Image getImage();

    void setWidth(int width);

    void setHeight(int height);

    void setX(int x);

    void setY(int y);

    void setImage(String imagePath);

    void move();
}
