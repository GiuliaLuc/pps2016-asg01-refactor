package utilities;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author Roberto Casadei
 */

public class Util {
    public static URL getResource(String path){
        return Util.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }
}
