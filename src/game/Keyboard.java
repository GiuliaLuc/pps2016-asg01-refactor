package game;

import application.Application;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        PlatformImpl platform = PlatformImpl.getInstance();

        if (platform.getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                if (platform.getxPos() == -1) {
                    platform.setxPos(0);
                    platform.setBackground1PosX(-50);
                    platform.setBackground2PosX(750);
                }
                platform.getMario().setMoving(true);
                platform.getMario().setToRight(true);
                platform.setMov(1);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (platform.getxPos() == 4601) {
                    platform.setxPos(4600);
                    platform.setBackground1PosX(-50);
                    platform.setBackground2PosX(750);
                }

                platform.getMario().setMoving(true);
                platform.getMario().setToRight(false);
                platform.setMov(-1);
            }

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                platform.getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        PlatformImpl platform = PlatformImpl.getInstance();
        platform.getMario().setMoving(false);
        platform.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
