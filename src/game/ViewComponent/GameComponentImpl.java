package game.ViewComponent;

import game.ViewComponent.GameComponent;
import gameComponent.BasedComponentImpl;
import gameComponent.objects.Block;
import gameComponent.objects.Coin;
import gameComponent.objects.Tunnel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucch on 18/03/2017.
 */
public class GameComponentImpl implements GameComponent {

    private List<BasedComponentImpl> components;
    private List<Coin> coins;
    private List<Tunnel> tunnelList;
    private List<Block> blockList;

    public GameComponentImpl(){
        this.components = new ArrayList<>();
        this.coins = new ArrayList<>();
        this.tunnelList = new ArrayList<>();
        this.blockList = new ArrayList<>();

    }

    @Override
    public List<Tunnel> getTunnelList(){
        this.tunnelList.add(new Tunnel(600, 230));
        this.tunnelList.add(new Tunnel(1000, 230));
        this.tunnelList.add(new Tunnel(1600, 230));
        this.tunnelList.add(new Tunnel(1900, 230));
        this.tunnelList.add(new Tunnel(2500, 230));
        this.tunnelList.add(new Tunnel(3000, 230));
        this.tunnelList.add(new Tunnel(3800, 230));
        this.tunnelList.add(new Tunnel(4500, 230));

        return tunnelList;
    }

    @Override
    public List<Block> getBlockList(){
        this.blockList.add(new Block(400, 180));
        this.blockList.add(new Block(1200, 180));
        this.blockList.add(new Block(1270, 170));
        this.blockList.add(new Block(1340, 160));
        this.blockList.add(new Block(2000, 180));
        this.blockList.add(new Block(2600, 160));
        this.blockList.add(new Block(2650, 180));
        this.blockList.add(new Block(3500, 160));
        this.blockList.add(new Block(3550, 140));
        this.blockList.add(new Block(4000, 170));
        this.blockList.add(new Block(4200, 200));
        this.blockList.add(new Block(4300, 210));

        return blockList;
    }

    @Override
    public List<Coin> getCoinList(){
        this.coins.add(new Coin(402, 145));
        this.coins.add(new Coin(1202, 140));
        this.coins.add(new Coin(1272, 95));
        this.coins.add(new Coin(1342, 40));
        this.coins.add(new Coin(1650, 145));
        this.coins.add(new Coin(2650, 145));
        this.coins.add(new Coin(3000, 135));
        this.coins.add(new Coin(3400, 125));
        this.coins.add(new Coin(4200, 145));
        this.coins.add(new Coin(4600, 40));

        return this.coins;
    }

    @Override
    public List<BasedComponentImpl> getComponents(){
        this.components.addAll(this.getTunnelList());
        this.components.addAll(this.getBlockList());

        return this.components;
    }




}
