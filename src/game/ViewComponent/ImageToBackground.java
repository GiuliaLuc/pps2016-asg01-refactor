package game.ViewComponent;

import utilities.ImagePath;
import utilities.Util;

import java.awt.*;

/**
 * Created by lucch on 18/03/2017.
 */
public class ImageToBackground {

    private Image imageBackground1;
    private Image imageBackground2;
    private Image castle;
    private Image startImage;
    private Image imageCastle;
    private Image imageFlag;

    public ImageToBackground(){
        this.imageBackground1 = Util.getImage(ImagePath.IMG_BACKGROUND);
        this.imageBackground2 = Util.getImage(ImagePath.IMG_BACKGROUND);
        this.castle = Util.getImage(ImagePath.IMG_CASTLE);
        this.startImage = Util.getImage(ImagePath.START_ICON);
        this.imageCastle = Util.getImage(ImagePath.IMG_CASTLE_FINAL);
        this.imageFlag = Util.getImage(ImagePath.IMG_FLAG);
    }

    public Image getImageBackground1() {
        return imageBackground1;
    }

    public Image getImageBackground2() {
        return imageBackground2;
    }

    public Image getCastle() {
        return castle;
    }

    public Image getStartImage() {
        return startImage;
    }

    public Image getImageCastle() {
        return imageCastle;
    }

    public Image getImageFlag() {
        return imageFlag;
    }
}
