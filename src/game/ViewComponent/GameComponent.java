package game.ViewComponent;

import gameComponent.BasedComponentImpl;
import gameComponent.objects.Block;
import gameComponent.objects.Coin;
import gameComponent.objects.Tunnel;

import java.util.List;

/**
 * Created by lucch on 18/03/2017.
 */
public interface GameComponent {
    List<Tunnel> getTunnelList();

    List<Block> getBlockList();

    List<Coin> getCoinList();

    abstract List<BasedComponentImpl> getComponents();
}
