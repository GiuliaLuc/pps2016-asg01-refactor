package game;

public class Refresh implements Runnable {

    private final int PAUSE = 3;

    public void run() {
        PlatformImpl platform = PlatformImpl.getInstance();
        while (true) {
            platform.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
