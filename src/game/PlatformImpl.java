package game;

import game.ViewComponent.GameComponent;
import game.ViewComponent.GameComponentImpl;
import game.ViewComponent.ImageToBackground;
import gameComponent.characters.*;

import java.awt.*;
import java.util.List;

import javax.swing.*;

import gameComponent.BasedComponentImpl;
import gameComponent.objects.Coin;
import utilities.ImagePath;

@SuppressWarnings("serial")
public class PlatformImpl extends JPanel implements Platform {

    private static PlatformImpl platformSingleton = null;


    public static synchronized PlatformImpl getInstance() {
        if (platformSingleton == null) {
            platformSingleton = new PlatformImpl();
        }
        return platformSingleton;
    }

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;

    private GameComponent gameComponent;
    private ImageToBackground imageToBackground;

    private Mario mario;
    private Mushroom mushroom;
    private Turtle turtle;

    private List<BasedComponentImpl> components;
    private List<Coin> coins;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private PlatformImpl() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imageToBackground = new ImageToBackground();

        this.gameComponent = new GameComponentImpl();
        this.components = gameComponent.getComponents();
        this.coins = gameComponent.getCoinList();

        this.mario = new Mario(300, 245);
        this.mushroom = new Mushroom(800, 263);
        this.turtle = new Turtle(950, 243);

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    @Override
    public Mario getMario() {
        return mario;
    }

    @Override
    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    @Override
    public int getHeightLimit() {
        return heightLimit;
    }

    @Override
    public int getMov() {
        return mov;
    }

    @Override
    public int getxPos() {
        return xPos;
    }

    @Override
    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    @Override
    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    @Override
    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    @Override
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    @Override
    public void setMov(int mov) {
        this.mov = mov;
    }

    @Override
    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    @Override
    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (int i = 0; i < components.size(); i++) {
            if (this.mario.isNearby(this.components.get(i)))
                this.mario.contactGameComponent(this.components.get(i));

            if (this.mushroom.isNearby(this.components.get(i)))
                this.mushroom.contact(this.components.get(i));

            if (this.turtle.isNearby(this.components.get(i)))
                this.turtle.contact(this.components.get(i));
        }

        for (int i = 0; i < coins.size(); i++) {
            if (this.mario.contactPiece(this.coins.get(i))) {
                Audio.playSound(ImagePath.AUDIO_MONEY);
                this.coins.remove(i);
            }
        }

        if (this.mushroom.isNearby(turtle)) {
            this.mushroom.contact(turtle);
        }
        if (this.turtle.isNearby(mushroom)) {
            this.turtle.contact(mushroom);
        }
        if (this.mario.isNearby(mushroom)) {
            this.mario.contactCharacter(mushroom);
        }
        if (this.mario.isNearby(turtle)) {
            this.mario.contactCharacter(turtle);
        }

        this.updateBackgroundOnMovement();

        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < components.size(); i++) {
                components.get(i).move();
            }

            for (int i = 0; i < coins.size(); i++) {
                this.coins.get(i).move();
            }

            this.mushroom.move();
            this.turtle.move();
        }

        g2.drawImage(imageToBackground.getImageBackground1(), this.background1PosX, 0, null);
        g2.drawImage(imageToBackground.getImageBackground2(), this.background2PosX, 0, null);
        g2.drawImage(imageToBackground.getCastle(), 10 - this.xPos, 95, null);
        g2.drawImage(imageToBackground.getStartImage(), 220 - this.xPos, 234, null);

        for (int i = 0; i < components.size(); i++) {
            g2.drawImage(this.components.get(i).getImage(), this.components.get(i).getX(),
                    this.components.get(i).getY(), null);
        }

        for (int i = 0; i < coins.size(); i++) {
            g2.drawImage(this.coins.get(i).imageOnMovement(), this.coins.get(i).getX(),
                    this.coins.get(i).getY(), null);
        }

        g2.drawImage(imageToBackground.getImageFlag(), FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(imageToBackground.getImageCastle(), CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario.isJumping()) {
            g2.drawImage(this.mario.doJump(), this.mario.getX(), this.mario.getY(), null);
        } else
            g2.drawImage(this.mario.walk(ImagePath.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);

        if (this.mushroom.isAlive()) {
            g2.drawImage(this.mushroom.walk(ImagePath.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.mushroom.getX(), this.mushroom.getY(), null);
        } else
            g2.drawImage(this.mushroom.deadImage(), this.mushroom.getX(), this.mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);

        if (this.turtle.isAlive()) {
            g2.drawImage(this.turtle.walk(ImagePath.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.turtle.getX(), this.turtle.getY(), null);
        } else
            g2.drawImage(this.turtle.deadImage(), this.turtle.getX(), this.turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);
    }
}
