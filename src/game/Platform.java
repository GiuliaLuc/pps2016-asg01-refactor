package game;

import gameComponent.characters.Mario;

import java.awt.*;

/**
 * Created by lucch on 18/03/2017.
 */
public interface Platform {

    Mario getMario();

    int getFloorOffsetY();

    int getHeightLimit();

    int getMov();

    int getxPos();

    void setBackground2PosX(int background2PosX);

    void setFloorOffsetY(int floorOffsetY);

    void setHeightLimit(int heightLimit);

    void setxPos(int xPos);

    void setMov(int mov);

    void setBackground1PosX(int x);

    void updateBackgroundOnMovement();

    void paintComponent(Graphics g);

}
