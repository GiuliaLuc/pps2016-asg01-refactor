package application;

import game.PlatformImpl;
import game.Refresh;

import javax.swing.JFrame;

public class Application {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";
    public static PlatformImpl viewport;

    public static void main(String[] args) {
        JFrame mainFrame = new JFrame(WINDOW_TITLE);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(true);
        mainFrame.setAlwaysOnTop(true);

        viewport = PlatformImpl.getInstance();
        mainFrame.setContentPane(viewport);
        mainFrame.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

}
